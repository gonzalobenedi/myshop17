<?php

use Illuminate\Database\Seeder;

class FamiliesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('families')->insert([
        	'code' => 'MICR',
        	'name' => 'microprocesadores'
        	]);
        DB::table('families')->insert([
        	'code' => 'PERI',
        	'name' => 'perifericos'
        	]);
        DB::table('families')->insert([
        	'code' => 'OKOK',
        	'name' => 'okidoki'
        	]);
        DB::table('families')->insert([
        	'code' => 'PEPE',
        	'name' => 'pepepepe'
        	]);
    }
}
