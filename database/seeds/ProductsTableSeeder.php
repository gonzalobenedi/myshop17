<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('products')->insert([
        	'code' => 'RA01',
        	'name' => 'Ratón inalámbrico',
            'price' => 10,
            'family_id' => 1
        	]);

        DB::table('products')->insert([
        	'code' => 'TE01',
        	'name' => 'Teclado inalámbrico',
            'price' => 15,
            'family_id' => 2
        	]);
    }
}
