@extends('layouts.app')

@section('content')
	<h1>Lista de productos</h1>
	<p><a href="/families/create">Añadir producto</a></p>
	<table class="table">
		<thead>
			<tr>
				<th>Id</th>
				<th>Código</th>
				<th>Nombre</th>
                <th>Precio</th>
                <th>Failia</th>
				<th>Acciones</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($products as $product)
				<tr>
					<td> {{ $product['id'] }} </td>
					<td> {{ $product['code'] }} </td>
					<td> {{ $product['name'] }} </td>
                    <td> {{ $product['price'] }} </td>
                    <td> {{ $product->family->name }} </td>
					<td>
						<form action="/families/{{$product->id}}" method="post">
						<a href="/families/{{ $product->id }}/edit">Editar</a>

							{{ csrf_field() }}
							<input type="hidden" name="_method" value="DELETE">
							<input type="submit" name="submit" value="Borrar">
							<a href="/families/{{ $product->id }}">Ver</a>
						</form>
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>
@endsection('content')
