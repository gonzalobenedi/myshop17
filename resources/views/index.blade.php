@extends('layouts.app')

@section('content')
	<h1>Lista de Familias</h1>
	<table class="table">
		<thead>
			<tr>
				<th>Id</th>
				<th>Codigo</th>
				<th>Nombre</th>

			</tr>
		</thead>
		<tbody>
			@foreach ($families as $family)
				<tr>
					<td> {{ $family['id'] }} </td>
				</tr>
			@endforeach
		</tbody>
	</table>
@endsection('content')