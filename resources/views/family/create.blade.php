@extends('layouts.app')

@section('content')
	<h1>Creación de una nueva familia</h1>


    <form class="" action="/families" method="post">
        {{ csrf_field() }}
        <div class="form-group">
            <label>Código:</label>
            <input type="text" name="code" value="{{old('code')}}">{{ $errors->first('code') }}
        </div>
        <div class="form-group">
            <label>Nombre:</label>
            <input type="text" name="name" value="{{old('name')}}">{{ $errors->first('name') }}
        </div>
        <div class="form-group">
            <input type="submit" name="submit" value="Añadir">
        </div>
    </form>


@endsection('content')
