@extends('layouts.app')

@section('content')
	<h1>Lista de Familias</h1>
	<table class="table table-striped">
		<thead>
			<tr>
				<th>Id</th>
				<th>Codigo</th>
				<th>Nombre</th>

			</tr>
		</thead>
		<tbody id="tbodyMain">
				<tr>
					<td> content </td>
				</tr>
		</tbody>
	</table>
@endsection('content')

@section('scripts')
	<script type="text/javascript">
	$.ajax({
		url: '/family', // url del recurso
		type: "get", // podría ser get, post, put o delete.
		data: {}, // datos a pasar al servidor, en caso de necesitarlo
		success: function (r) {
			// aquí trataríamos la respuesta del servidor
		}
});
	</script>
@stop
