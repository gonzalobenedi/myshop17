@extends('layouts.app')

@section('content')
	<h1>Lista de Familias</h1>
	<p><a href="/families/create">Añadir familia</a></p>
	<table class="table">
		<thead>
			<tr>
				<th>Id</th>
				<th>Código</th>
				<th>Nombre</th>
				<th>Acciones</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($families as $family)
				<tr>
					<td> {{ $family['id'] }} </td>
					<td> {{ $family['code'] }} </td>
					<td> {{ $family['name'] }} </td>
					<td>
						<form action="/families/{{$family->id}}" method="post">
						<a href="/families/{{ $family->id }}/edit">Editar</a>

							{{ csrf_field() }}
							<input type="hidden" name="_method" value="DELETE">
							<input type="submit" name="submit" value="Borrar">
							<a href="/families/{{ $family->id }}">Ver</a>
						</form>
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>
@endsection('content')
