@extends('layouts.app')

@section('content')
	<h1>Edición de la familia</h1>


    <form class="" action="/families/{{$family->id}}" method="post">
        {{ csrf_field() }}
        <input type="hidden" name="_method" value="PUT">

        <div class="form-group">
            <label>Id:</label>
            <input type="text" name="id" value="{{$family->id}}" readonly="true">
        </div>
        <div class="form-group">
            <label>Código:</label>
            <input type="text" name="code" value="{{$family->code}}">{{ $errors->first('code') }}
        </div>
        <div class="form-group">
            <label>Nombre:</label>
            <input type="text" name="name" value="{{$family->name}}">{{ $errors->first('name') }}
        </div>
        <div class="form-group">
            <input type="submit" name="submit" value="Actualizar">
        </div>
    </form>


@endsection('content')
